export default {
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'UFO Coin Roadmap (Uniform Fiscal Object)',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
      {
        href:
          'https://fonts.googleapis.com/css2?family=Manrope:wght@700&family=Oswald:wght@700&family=Rubik&display=swap',
        rel: 'stylesheet',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/css/main.css', { src: '~/assets/css/main.css', lang: 'css' }],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    'nuxt-i18n',
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '73172488',
        webvisor: true,
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
      },
    ],
  ],

  generate: {
    dir: 'public',
  },

  i18n: {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: {
          twitter: 'https://twitter.com/fiscalobject',
          telegram: 'https://t.me/UFOCoin',
          github: 'https://github.com/fiscalobject/ufo',
          discord: 'https://discord.gg/p3VQwFX',
          coinmarketcap:
            'https://coinmarketcap.com/currencies/uniform-fiscal-object/',
          ufobject_com: 'https://ufobject.com',

          presentation: 'Download presentation',
          privacyPolicy: 'Privacy Policy',
          events: [
            [
              {
                date: '3 Jan 2014',
                title: 'Genesis block',
                desc: 'UFO Blockchain started.',
              },
              {
                date: '11 Feb 2014',
                title: 'UFO 0.8',
                desc: 'Codebase updated to match Bitcoin 0.8.',
                // address: { name: 'Веб-сайт: ', link: 'edge.app' },
              },
              {
                date: '11 Feb 2014',
                title: 'Bitcointalk thread started.',
                address: {
                  name: 'Website: ',
                  link: 'bitcointalk.org/index.php?topic=460522',
                },
              },
              {
                date: '17 Jul 2014',
                title: 'UFO 0.8.7.1',
                desc:
                  'Hardforked at block 171900. Fixed NGW difficulty issues.',
              },
              {
                date: '26 Jul 2014',
                title: 'Peter Bushnell joined the team',
                desc:
                  'Peter Bushnell, the creator of Feathercoin, joined the team.',
                address: {
                  name: 'Announcement: ',
                  link: 'twitter.com/UFOCoins/status/493064382331768832',
                },
              },
              {
                date: '19 Oct 2014',
                title: 'UFO 0.8.8',
                desc:
                  'Hard fork to NeoScrypt and enable BIP66 in Ufo Core 0.8.8.',
              },
            ],
            [
              {
                date: '31 Jan 2017',
                title: 'UFO listed at Coinexchange',
              },
              {
                date: '1 Feb 2017',
                title: 'Listed in Cryptoid explorer',
                desc: 'Popular blockchain explorer supports UFO blockchain.',
                address: {
                  name: 'Website: ',
                  link: 'chainz.cryptoid.info/ufo/',
                },
              },
              {
                date: '31 Aug 2017',
                title: 'UFO 0.9',
                desc: 'Codebase updated to match Bitcoin Core 0.9.',
              },
              {
                date: '3 Oct 2017',
                title: 'UFO 0.10',
                desc: 'Codebase updated to match Bitcoin Core 0.10.',
              },
            ],
            [
              {
                date: '7 Mar 2018',
                title: 'UFO 0.13',
                desc:
                  'Codebase updated to match Bitcoin Core 0.13 with activated SegWit.',
              },
              {
                date: '3 Jun 2018',
                title: 'UFO 0.16',
                desc:
                  "SegWit addresses (started with U) are now by default. Bech32 support for 'uf1' addresses. HD wallet by default. Codebase updated to match Bitcoin Core 0.16.",
              },
              {
                date: '16 Jun 2018',
                title: 'UFO 0.16.1',
                desc:
                  'Optimized Neoscrypt for SSE2. Codebase updated to match Bitcoin Core 0.16.1.',
              },
              {
                date: '24 Jun 2018',
                title: 'UFO listed in Edge app',
                desc:
                  'Edge is very secure, non-custodial and popular wallet with user-friendly mobile app and a rich set of features.',
                address: { name: 'Website: ', link: 'edge.app' },
              },
              {
                date: '22 Sep 2018',
                title: 'UFO 0.16.3',
                desc:
                  'Security update against possible Bitcoin Core DoS attack.',
              },
              {
                date: '15 Oct 2018',
                title: 'UFO 0.17',
                desc: 'Codebase updated to match Bitcoin Core 0.17.',
              },
              {
                date: '20 Nov 2018',
                title: 'Uniform Asset Layer',
                desc:
                  'Test version of Uniform Asset Layer allows issuance of OMNI compatible tokens.',
                address: { name: 'Website: ', link: 'uniasset.org' },
              },
              {
                date: '15 Dec 2018',
                title: 'UFOdex',
                desc:
                  'Experimental UFOdex exchange for secure exchanging via atomic swaps: regular interface, non-custodial.',
                address: { name: 'Website: ', link: 'ufodex.io' },
              },
            ],
            [
              {
                date: '1 Jul 2019',
                title: 'Listed in CoinKit',
                desc:
                  'UFO listed in CoinKit bot which allows sending of coins on public plaftform: Twitter, Telegram, Discord.',
                address: { name: 'Website: ', link: 'coinkit.de' },
              },
              {
                date: '30 Jul 2019',
                title: 'UFO 0.18',
                desc: 'Codebase updated to match Bitcoin Core 0.18.',
              },
              {
                date: '12 Feb 2019',
                title: 'UniAsset',
                desc:
                  'Beta versoin of UniAsset released for mainnet. Allows issuance of OMNI compatible tokens. Provides exchange and distribution based on UFO blockchain.',
                address: { name: 'Веб-сайт: ', link: 'uniasset.org' },
              },
            ],
            [
              {
                date: '2 Jul 2020',
                title: 'Listed at Graviex exchange',
                address: { name: 'Website: ', link: 'graviex.net' },
              },
              {
                date: '1 Dec 2020',
                title: 'Listed at OCCE exchange',
                address: { name: 'Website: ', link: 'occe.io' },
              },
            ],
            [
              {
                date: '26 Feb 2021',
                title: 'Official blockchain explorer for UFO.',
                address: { name: 'Website: ', link: 'explorer.ufobject.com' },
              },
              {
                date: '1 Mar 2021',
                title: 'Listed at Bololex exchange',
                address: { name: 'Website: ', link: 'Bololex.com' },
              },
              {
                date: '15 Mar 2021',
                title: 'Telegram swap bot',
                desc: 'Telegram bot for P2P UFO exchange.',
              },
              {
                date: 'April 2021',
                title: 'Official mining pool comes out of beta.',
                address: { name: 'Website: ', link: 'pool.ufobject.com' },
              },
              {
                date: 'Mar-Apr 2021',
                title: 'Web wallet for UFO',
                desc:
                  'Beta version of UFO web wallet. Features in development: support for UFO tokens, a platform for integration with services that accept UFO, secure exchange via atomic swaps.',
                address: { name: 'Веб-сайт: ', link: 'wallet.calcium.network' },
              },
              {
                date: 'Apr-May 2021',
                title: 'UFO 0.21',
                desc: 'Codebase update to match Bitcoin Core 0.21.',
              },
              {
                date: '2021',
                title: 'Update OMNI in UFO blockchain',
              },
              {
                date: '2021',
                title: 'OMNI transactions explorer for UFO blockchain',
              },
              {
                date: '2021',
                title: 'Mobile wallet for UFO and OMNI transactions',
              },
              {
                date: '2021',
                title: 'Ecosystem for issuing utility tokens',
                desc:
                  'Issuing and managing different tokens over UFO blockchain.',
              },
              {
                date: 'Late 2021',
                title: 'TapRoot activation.',
              },
            ],
            [
              {
                date: 'Late 2021 - Early 2022',
                title: 'Public stablecoin',
                desc:
                  'Release of public stablecoin will enable quick and comfortable user experience for stablecoin payments over UFO blockchain.',
              },
              {
                date: 'Late 2021 - Early 2022',
                title: 'Mobile messenger with wallet and authenticator',
                desc:
                  'Mobile messenger with wallet and authenticator in development. Decentralized over the Calcium.Network nodes.',
              },
              {
                date: '2022',
                title: 'Storage services',
                desc:
                  'Users will be able to  provide fee space and get paid in UFO.',
              },
            ],
            [
              {
                date: '2023',
                title: 'Distributed computing services',
                desc:
                  'Users will be able to provide CPU time and get paid in UFO.',
              },
              {
                date: '2023',
                title: 'To be continued...',
              },
            ],
            [],
          ],
        },
        ru: {
          twitter: 'https://twitter.com/UFObject_ru',
          telegram: 'https://t.me/UFObjectRU',
          github: 'https://github.com/fiscalobject/ufo',
          discord: 'https://discord.gg/p3VQwFX',
          coinmarketcap:
            'https://coinmarketcap.com/ru/currencies/uniform-fiscal-object/',
          ufobject_com: 'https://ufobject.com/ru',

          presentation: 'Скачать презентацию',
          privacyPolicy: 'Политика конфиденциальности',
          events: [
            [
              {
                date: '3 января 2014',
                title: 'Генезис блок',
                desc: 'Энтропия запущена.',
              },
              {
                date: '11 февраля 2014',
                title: 'UFO 0.8',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.8',
                // address: { name: 'Веб-сайт: ', link: 'edge.app' },
              },
              {
                date: '11 февраля 2014',
                title: 'Тред на Bitcointalk',
                address: {
                  name: 'Веб-сайт: ',
                  link: 'bitcointalk.org/index.php?topic=460522',
                },
              },
              {
                date: '17 июля 2014',
                title: 'UFO 0.8.7.1',
                desc:
                  'Хардфорк на блоке 171900. Исправлены проблемы сложности NGW.',
              },
              {
                date: '26 июля 2014',
                title: 'Приход в команду Питера Башнелла',
                desc:
                  'К команде разработчиков присоединился создатель Feathercoin – Питер Башнелл.',
                address: {
                  name: 'Анонс:',
                  link: 'twitter.com/UFOCoins/status/493064382331768832',
                },
              },
              {
                date: '19 октября 2014',
                title: 'UFO 0.8.8',
                desc: 'Хард-форк на алгоритм NeoScrypt, включение BIP66',
              },
            ],
            [
              {
                date: '31 января 2017',
                title: 'Листинг на бирже coinexchange',
              },
              {
                date: '1 февраля 2017',
                title: 'Листинг проводника в Cryptoid',
                desc: 'Размещение проводника в популярном блокчейн-эксплорере.',
                address: {
                  name: 'Веб-сайт: ',
                  link: 'chainz.cryptoid.info/ufo/',
                },
              },
              {
                date: '31 августа 2017',
                title: 'UFO 0.9',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.9',
              },
              {
                date: '3 октября 2017',
                title: 'UFO 0.10',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.10',
              },
            ],
            [
              {
                date: '7 марта 2018',
                title: 'UFO 0.13',
                desc: 'Выходит UFO Core 0.13 с активированным SegWit.',
              },
              {
                date: '3 июня 2018',
                title: 'UFO 0.16',
                desc:
                  'По умолчанию используются адреса SegWit, которые начинаются с U начиная с 0.15 версии, имеет поддержку адресов Bech32 uf1, HD по умолчанию и множество других обновлений по сравнению с последним выпуском.',
              },
              {
                date: '16 июня 2018',
                title: 'UFO 0.16.1',
                desc:
                  'Релиз с новым обновлениями от Биткоина 0.16.1 Это обновление  включает в себя оптимизацию NeoScrypt SSE2.',
              },
              {
                date: '24 июня 2018',
                title: 'Размещениие UFO в кошельке Edge',
                desc:
                  'Когда-то edge был всего-лишь стартапом, а теперь это очень популярный и довольно увесистый кошелек с сильным функционалом и хорошей защитой.',
                address: { name: 'Веб-сайт: ', link: 'edge.app' },
              },
              {
                date: '22 сентября 2018',
                title: 'UFO 0.16.3',
                desc:
                  'Обновление против уязвимости DoS, обнаруженной в Биткоине.',
              },
              {
                date: '15 октября 2018',
                title: 'UFO 0.17',
                desc: 'Актуализация UFO core под Bitcoin core 0.17',
              },
              {
                date: '20 ноября 2018',
                title: 'Unform Asset Layer',
                desc:
                  'Тестовая версия технологии Uniform Asset Layer позволяет выпускать токены, обеспечивает механизм распространения и обмена, построенный на базе блокчейна UFObject. Основан на коде  Ufobject 0.14 и Omnicore.',
                address: { name: 'Веб-сайт: ', link: 'uniasset.org' },
              },
              {
                date: '15 декабря 2018',
                title: 'UFOdex',
                desc:
                  'Экспериментальная биржа UFOdex для безопасного обмена через atomic swap - с привычным пользовательским интерфейсом, не имеет доступа к средствам пользователей.',
                address: { name: 'Веб-сайт: ', link: 'ufodex.io' },
              },
            ],
            [
              {
                date: '1 июля 2019',
                title: 'Листинг в CoinKit',
                desc:
                  'Размещение UFO в CoinKit - боте, позволяющем передавать ценности в мессенджерах: Twitter, Telegram, Discord.',
                address: { name: 'Веб-сайт: ', link: 'coinkit.de' },
              },
              {
                date: '30 июля 2019',
                title: 'UFO 0.18',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.18',
              },
              {
                date: '12 февраля 2019',
                title: 'UniAsset',
                desc:
                  'Бета версия технологии UniAsset выпущена в основной сети. Позволяет выпускать токены, обеспечивает механизм распространения и обмена, построенный на базе блокчейна UFobject. Основан на коде  Ufobject 0.14 и доработанном коде Omnicore.',
                address: { name: 'Веб-сайт: ', link: 'uniasset.org' },
              },
            ],
            [
              {
                date: '2 июля 2020',
                title: 'Листинг на Graviex',
                address: { name: 'Веб-сайт: ', link: 'graviex.net' },
              },
              {
                date: '1 декабря 2020',
                title: 'Листинг в OCCE',
                address: { name: 'Веб-сайт: ', link: 'occe.io' },
              },
            ],
            [
              {
                date: '26 февраля 2021',
                title: 'Запуск официального эксплорера транзакций UFO.',
                address: { name: 'Веб-сайт: ', link: 'explorer.ufobject.com' },
              },
              {
                date: '1 марта 2021',
                title: 'Листинг в бирже Bololex',
                address: { name: 'Веб-сайт: ', link: 'Bololex.com' },
              },
              {
                date: '15 марта 2021',
                title: 'Telegram swap bot',
                desc: 'Телеграм бот по p2p обмену рублей на UFO',
              },
              {
                date: 'Апрель 2021',
                title: 'Запуск официального пула',
                address: { name: 'Веб-сайт: ', link: 'pool.ufobject.com' },
              },
              {
                date: 'Март-апрель 2021',
                title: 'Web колешек UFO',
                desc:
                  '(Альфа версия, бета на подходе). Кошелек для хранения UFO, поддержка токенов на блокчейне UFO, платформа для интеграции сервисов с оплатой в UFO, безопасный обмен через атомарные свопы',
                address: { name: 'Веб-сайт: ', link: 'wallet.calcium.network' },
              },
              {
                date: 'Март-апрель 2021',
                title: 'UFO 0.21',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.21',
              },
              {
                date: 'Март-апрель 2021',
                title: 'UFO 0.21',
                desc: 'Актуализация UFO core под Bitcoin core версии 0.21',
              },
              {
                date: '2021',
                title: 'Интеграция OMNI в UFO',
              },
              {
                date: '2021',
                title: 'Проводник OMNI транзакций',
              },
              {
                date: '2021',
                title: 'Мобильный кошелек для OMNI транзакций',
              },
              {
                date: '2021',
                title: 'Экосистема для выпуска утилитарных токенов',
                desc:
                  'Экосистема для выпуска и управления различными типами токенов, где как у эмитентов, так и у конечных пользователей будет свой функционал.',
              },
              {
                date: 'Конец 2021',
                title: 'Активация TapRoot',
              },
            ],
            [
              {
                date: 'Конец 2021 - начало 2022',
                title: 'Публичный RUB стейблкойн',
                desc:
                  'Выпуск рублевого стейблкойна позволит очень быстро дешево пользоваться крипторублём.',
              },
              {
                date: 'Конец 2021 - начало 2022',
                title: 'Мобильный мессенджер/кошелек/аутентификатор',
                desc:
                  'Мобильный мессенджер/кошелек/аутентификатор (в разработке). Децентрализован за счет нод сети Calcium.',
              },
              {
                date: '2022',
                title: 'Сервисы хранения',
                desc:
                  'Сдача в аренду пользователями свободного места на диске. Оплата за предоставление ресурсов пользователями в UFO.',
              },
            ],
            [
              {
                date: '2023',
                title: 'Сервисы распределения',
                desc:
                  'Сдача в аренду пользователями простаивающих мощностей. Оплата за предоставление ресурсов пользователями в UFO.',
              },
              {
                date: '2023',
                title: 'Продожение следует...',
              },
            ],
            [],
          ],
        },
      },
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
